import React from 'react';

const remote = require('electron').remote;
const storage = remote.require('electron-json-storage');

export class Settings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      gamePath: ''
    };
  }

  async getConfigKey(key) {
    return new Promise((resolve, reject) => {
      storage.get(key, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data && data.value);
        }
      })
    })
  }

  async componentDidMount() {
    const dataPath = storage.getDataPath();
    console.log(dataPath);

    const gamePath = await this.getConfigKey('gamePath');
    console.log(gamePath);
    this.setState({
      gamePath
    });
  }

  async updateValue(field, value) {
    console.log('updating', field, value);
    await storage.set(field, {
      value
    });
  }

  render() {
    return (
      <div className="input-group">
        <input id="gamePath" defaultValue={this.state.gamePath} placeholder="Game path" onChange={(e) => this.updateValue('gamePath', e.target.value)}/>
        <label htmlFor="gamePath">Path to EscapeFromTarkov.exe</label>
      </div>
    );
  }

}
