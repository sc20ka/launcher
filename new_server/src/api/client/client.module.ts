import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {DeflateMiddleware} from '../middleware/deflateMiddleware';
import {LanguagesController} from './languages.controller';
import {GameModule} from './game/game.module';
import {APP_FILTER} from '@nestjs/core';
import {EftExceptionFilter} from '../middleware/eftException.filter';
import {ServicesModule} from '../../services/services.module';

@Module({
    imports: [GameModule, ServicesModule],
    controllers: [LanguagesController],
    providers: [
        {
            provide: APP_FILTER,
            useClass: EftExceptionFilter,
        },
    ]
})
export class ClientModule implements NestModule {
    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
        return consumer
            .apply(DeflateMiddleware)
            .forRoutes('*');
    }
}
