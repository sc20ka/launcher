import {Controller, Logger, Param, Post, UseInterceptors} from '@nestjs/common';
import {GetLanguagesResponseDto} from './dto/getLanguagesResponse.dto';
import {InflateSerializerInterceptor} from '../middleware/inflateSerializerInterceptor';

const menuLanguage = require('../../../../data/locale/en/menu.json');
const globalLanguage = require('../../../../data/locale/en/global.json');

@UseInterceptors(InflateSerializerInterceptor)
@Controller('client')
export class LanguagesController {

    private readonly logger = new Logger(LanguagesController.name);

    @Post('languages')
    getLanguages(): GetLanguagesResponseDto {
        this.logger.debug('Client is requesting languages');
        return [
            {
                Name: 'English',
                ShortName: 'en'
            },
            {
                Name: 'Русский',
                ShortName: 'ru'
            }
        ];
    }

    @Post('menu/locale/:language')
    getMenuLanguage(@Param('language') language: string): {} {
        this.logger.debug(`Client is requesting menu language for ${language}`);
        return menuLanguage;
    }

    @Post('locale/:language')
    getLanguage(@Param('language') language: string): {} {
        this.logger.debug(`Client is requesting language for ${language}`);
        return globalLanguage;
    }

}

