export abstract class GetLanguagesResponseDto extends Array<{
    ShortName: string;
    Name: string;
}> {

}
