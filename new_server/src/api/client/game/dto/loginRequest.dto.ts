export abstract class LoginRequestDto {


    public email!: string;
    public pass!: string;
    public device_id!: string;
    public develop!: boolean;
    public sec!: number;

    public version!: {
        major: string;
        minor: string;
        game: string;
        backend: string;
        taxonomy: string;
    };

}
