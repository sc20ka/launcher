export abstract class LoginResponseDto {
    /*
    "token": "token_1337",
    "aid": 1337,
    "lang": "en",
    "languages": {
      "en": "English"
    },
    "ndaFree": true,
    "queued": false,
    "taxonomy": 341,
    "activeProfileId": "5c71b934354682353958e984",
    "backend": {
      "Trading": "' + backendUrl + '",
      "Messaging": "' + backendUrl + '",
      "Main": "' + backendUrl + '",
      "RagFair": "' + backendUrl + '"
    },
    "utc_time": 1337,
    "totalInGame": 0,
    "twitchEventMember": false
    */

    public token!: string;
    public aid!: number;
    public lang!: string;
    public languages!: { [shortName: string]: string };
    public ndaFree!: boolean;
    public queued!: boolean;
    public taxonomy!: number;
    public activeProfileId!: string;
    public backend!: {
        Trading: string;
        Messaging: string;
        Main: string;
        RagFair: string;
    };
    public utc_time!: number;
    public totalInGame!: number;
    public twitchEventMember!: boolean;

}
