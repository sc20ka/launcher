export abstract class GenericClientResponseDto<TData = any> {

    public err!: number;
    public errmsg?: string;
    public data?: TData;
    public crc: number = 0;

}
