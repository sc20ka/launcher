export interface Info {
    Nickname: string;
    LowerNickname: string;
    Side: string;
    Voice: string;
    Level: number;
    Experience: number;
    RegistrationDate: number;
    GameVersion: string;
    AccountType: number;
    MemberCategory: number;
    lockedMoveCommands: boolean;
    LastTimePlayedAsSavage: number;
    Settings: Settings;
    NeedWipe: boolean;
    GlobalWipe: boolean;
    NicknameChangeDate: number;
}

export interface Settings {
}

export interface CustomizationPart {
    path: string;
    rcid?: any;
}

export interface Customization {
    Head: CustomizationPart;
    Body: CustomizationPart;
    Feet: CustomizationPart;
    Hands: CustomizationPart;
}

export interface Hydration {
    Current: number;
    Maximum: number;
}

export interface Health {
    Current: number;
    Maximum: number;
}

export interface BodyPart {
    Health: Health;
}

export interface BodyParts {
    Head: BodyPart;
    Chest: BodyPart;
    Stomach: BodyPart;
    LeftArm: BodyPart;
    RightArm: BodyPart;
    LeftLeg: BodyPart;
    RightLeg: BodyPart;
}

export interface Health {
    Hydration: Hydration;
    Energy: Health;
    BodyParts: BodyParts;
}

export interface Item {
    _id: string;
    _tpl: string;
    parentId: string;
    slotId: string;
}

export interface FastPanel {
}

export interface Inventory {
    items: Item[];
    equipment: string;
    stash: string;
    questRaidItems: string;
    questStashItems: string;
    fastPanel: FastPanel;
}

export interface Skills {
    Common: any[];
    Mastering: any[];
    Points: number;
}

export interface SessionCounters {
    Items: any[];
}

export interface OverallCounters {
    Items: any[];
}

export interface Stats {
    SessionCounters: SessionCounters;
    OverallCounters: OverallCounters;
}

export interface ConditionCounters {
    Counters: any[];
}

export interface BackendCounters {
}

export interface LocalPlayerInfo {
    _id: string;
    aid: number;
    savage?: any;
    Info: Info;
    Customization: Customization;
    Health: Health;
    Inventory: Inventory;
    Skills: Skills;
    Stats: Stats;
    Encyclopedia?: any;
    ConditionCounters: ConditionCounters;
    BackendCounters: BackendCounters;
    InsuredItems: any[];
}

