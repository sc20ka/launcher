const withCss = require('@zeit/next-css');
const withLess = require('@zeit/next-less');

module.exports = withCss(
    withLess({
        lessLoaderOptions: {
            javascriptEnabled: true
        },
        javascriptEnabled: true,
        exportPathMap: function () {
            return {
                '/': {page: '/'}
            };
        }
    })
);
