import React from 'react';
import dynamic from 'next/dynamic';
import {Breadcrumb, Button, Descriptions, Divider, Empty, Layout, Select} from 'antd';
import {withRouter} from 'next/router'
import Link from 'next/link';
import './index.less';
//import { Popover, Button } from 'antd'; // popover Imports
import { Tooltip } from 'antd';

const DynamicReactJson = dynamic(import('react-json-view'), {ssr: false});

const itemsData = require('../../data/items');
const globalLocaleEn = require('../../data/locale/en/global');
const processedItemData = Object.keys(itemsData.data).map(id => ({
    id,
    data: itemsData.data[id]
}));

export default withRouter(class Home extends React.Component {

    static MAX_ENTRIES_IN_SEARCH = 50;

    static getInitialProps({query}) {
        return {query}
    }

    constructor(props) {
        super(props);
        this.state = {
			actuallySearchedFor: undefined,
            currentlySelected: undefined,
            searchResult: processedItemData.slice(0, Home.MAX_ENTRIES_IN_SEARCH)
        };
        this.onSearch = this.onSearch.bind(this);
        this.onSelect = this.onSelect.bind(this);
    }

    onSearch(search) {
        this.setState({
			actuallySearchedFor: search.toLowerCase(),
            searchResult: processedItemData
                .filter(item => (
                    item.data._name.toLowerCase().indexOf(search.toLowerCase()) !== -1 ||
                    item.data._id.toLowerCase().indexOf(search.toLowerCase()) !== -1 ||
                    item.data._parent.toLowerCase().indexOf(search.toLowerCase()) !== -1 ||
                    (globalLocaleEn.templates[item.data._id] && globalLocaleEn.templates[item.data._id].Name &&  globalLocaleEn.templates[item.data._id].Name.toLowerCase().indexOf(search.toLowerCase()) !== -1)
                ))
                .slice(0, Home.MAX_ENTRIES_IN_SEARCH)
        });
    }

    onSelect(selected) {
        this.props.router.push({
            pathname: '/',
            query: {
                q: selected
            }
        });
    }

    renderSelectedItem() {
        const selectedItem = processedItemData.find(pid => pid.id === this.props.query.q);
        if (selectedItem === undefined) {
            return (
                <Empty style={{paddingTop: '32px'}}/>
            );
        }
        const parentItem = processedItemData.find(item => item.id === selectedItem.data._parent);
        const localizedName = globalLocaleEn.templates[selectedItem.data._id] && globalLocaleEn.templates[selectedItem.data._id].Name || selectedItem.data._name || selectedItem.data._id;
        const localizedParentName = globalLocaleEn.templates[selectedItem.data._parent] && globalLocaleEn.templates[selectedItem.data._parent].Name || parentItem && (parentItem.data._name || parentItem.data._id);
        return (
            <React.Fragment>
                <Divider orientation="left">Item translated name: {localizedName}</Divider>
                <Descriptions bordered title="Item detailed information" border>
                    <Descriptions.Item label="ID">{selectedItem.data._id}</Descriptions.Item>
                    <Descriptions.Item label="Type">{selectedItem.data._type}</Descriptions.Item>
                    <Descriptions.Item label="Parent">
                        {parentItem && (
                            <Link href={{
                                pathname: '/',
                                query: {
                                    q: selectedItem.data._parent
                                }
                            }} target="_blank">
                                <Tooltip title="Open this parent as item">
									<Button type="link">{localizedParentName}</Button>
								</Tooltip>
                            </Link>
                        ) || <span>No parent</span>
						}
						<br />
						{selectedItem.data._parent}
                    </Descriptions.Item>
                </Descriptions>
				<div className='json-view-title'>_props</div>
                <div className='json-view'>
                    <DynamicReactJson
                        name={false}
                        src={selectedItem.data._props || {}}
                    />
                </div>
            </React.Fragment>
        );
    }

    renderSelectedItemHierarchy() {
        let currentItem = processedItemData.find(pid => pid.id === this.props.query.q);
        const breadcrumbItems = [];
        while (currentItem !== undefined) {
            const localizedName = globalLocaleEn.templates[currentItem.data._id] && globalLocaleEn.templates[currentItem.data._id].Name || currentItem.data._name || currentItem.data._id;
            breadcrumbItems.push(
                <Breadcrumb.Item key={currentItem.id}>
                    <Link href={{
                        pathname: "/",
                        query: {
                            q: currentItem.id
                        }
                    }}>
                        <Button type="link">{localizedName}</Button>
                    </Link>
                </Breadcrumb.Item>
            );
            currentItem = processedItemData.find(pid => pid.id === currentItem.data._parent);
        }
        return breadcrumbItems.reverse();
    }

    render() {
        const selectedItem = processedItemData.find(pid => pid.id === this.props.query.q);
        const localizedName = selectedItem && (globalLocaleEn.templates[selectedItem.data._id] && globalLocaleEn.templates[selectedItem.data._id].Name || selectedItem.data._name || selectedItem.data._id);
        return (
            <Layout className='layout'>
                <Layout.Header>
                    <h1 className='row header'>EFT Item Viewer - Displayed item template id: {this.props.query.q}</h1>
                </Layout.Header>
                <Layout.Content className='content'>
                    <Breadcrumb style={{margin: '16px 0'}}>
                        <Breadcrumb.Item>
                            <Link href={"/"}>
                                <Button type="link">Home</Button>
                            </Link>
                        </Breadcrumb.Item>
                        {this.renderSelectedItemHierarchy()}
                    </Breadcrumb>
                    <Layout className='inner-layout'>
                        <Layout.Content className='inner-content'>
                            <Select
                                showSearch
                                className='item-select'
                                placeholder="Select an item"
                                value={localizedName}
                                onSearch={this.onSearch}
                                onSelect={this.onSelect}
                                filterOption={() => true}
                            >
							
                                {this.state.searchResult.map((item, itemIndex) => {
									const isFoundInParent = (this.state.actuallySearchedFor == item.data._parent)?" (_parent)":"";
									const searchedItem = (this.state.actuallySearchedFor == item.data._id)?" (You search this)":"";
									const localizedParentName = globalLocaleEn.templates[item.data._parent] && globalLocaleEn.templates[item.data._parent].Name;

                                    const localizedName = globalLocaleEn.templates[item.data._id] && globalLocaleEn.templates[item.data._id].Name || item.data._name || item.data._id;
                                    return (
                                        <Select.Option
                                            key={item.data._id}
                                            value={item.data._id}
                                        >
										{localizedName}{isFoundInParent}{searchedItem} [{localizedParentName}]
                                        </Select.Option>
                                    );
                                })}
                            </Select>
                            <div className='data-view'>
                                {this.renderSelectedItem()}
                            </div>
                        </Layout.Content>
                    </Layout>
                </Layout.Content>
                <Layout.Footer style={{textAlign: 'center'}}>JustEmuTarkov ©2019 Created by
                    AmazingTurtle</Layout.Footer>
            </Layout>
        );
    }

});
